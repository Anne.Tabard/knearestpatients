%addpath('ExportFig')
clear all
model=load('All_R.mat','All_R');
field=fieldnames(model);
model=model.(field{1});
file='figure';
Y=model.X;
F=fieldnames(Y);
F={'RKneeAngles_1','RAnkleAngles_1','RHipAngles_1'};
Y2=[];
for i=1:length(F)
    Y2.(F{i})=Y.(F{i})(1,:);
end
Yid=model.Y;
[Id,dist]=FindKNearestPerson(model,Y2,10,10);
t=[0:100];
subplot(3,1,1)
plot(Y.(F{1})(Id{1},:)')
subplot(3,1,2)
plot(Y.(F{2})(Id{1},:)')
subplot(3,1,3)
plot(Y.(F{3})(Id{1},:)')
unique(model.Visite(Id{1}))
%PlotNearest(model,Y,Id,distcle,t,file,Yid)