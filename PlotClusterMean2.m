function PlotClusterMean2(Cluster,Y,t)
    %UNTITLED2 Summary of this function goes here
    %   Detailed explanation goes here
    listi=[1 2 1 4 3 1 3 4 4 1 1 2 4 2 2 1 2 2];
    listj=[5 4 6 6 1 4 4 1 3 3 2 1 4 3 2 1 5 6];
    listangle={'RPelvisAngles_2','RHipAngles_1','RPelvisAngles_3','RFootProgressAngles_3','LKneeAngles_1','RPelvisAngles_1','RKneeAngles_1','LAnkleAngles_1','LFootProgressAngles_3','LPelvisAngles_3','LPelvisAngles_2','LHipAngles_1','RAnkleAngles_1','LHipAngles_3','LHipAngles_2','LPelvisAngles_1','RHipAngles_2','RHipAngles_3'};
    cluster=unique(Cluster);
    figure('Position',[1 1 1600 900])
    
    for k=1:length(listi)
        i=listi(k);
        j=listj(k);
        stitle=listangle{k};
        ind=(i-1)*6+j;
        s=subplot(4,6,ind);
            hold all
        for p=1:length(unique(cluster))
            
            InCluster=find(Cluster==cluster(p));
            plot(t,mean(Y.(listangle{k})(InCluster,:),1))
            title(stitle);
            
        end
        hold off
    end
end

