function [Id,dist] = FindKNearestPerson(model,Y,k,lambda)
    %UNTITLED2 Summary of this function goes here
    %   Detailed explanation goes here
    listfield=fieldnames(Y);
    X=[];
    X2=[];
    dX=[];
    dX2=[];
    for i=1:length(listfield)
        X=[X model.X.(listfield{i})];
        dX=[dX diff(model.X.(listfield{i}),1,2)];
        X2=[X2 Y.(listfield{i})];
        dX2=[dX2 diff(Y.(listfield{i}),1,2)];
    end
    d=pdist2(X2,X);
    d2=pdist2(dX2,dX);
    dista=sqrt(d.^2+lambda*d2.^2);
    I=1:size(X2,1);
    myfun=createfunction(dista,Y,k);
    [Id,dist]=arrayfun(myfun,I);
    function fun=createfunction(dist,target,kin)
        fun=@f;
        function [pred,distout]=f(indice)
            dligne=dist(indice,:);
            [S,Ind]=sort(dligne);
            pred={Ind(1:kin)};
            distout={S(1:kin)};
        end
    end
end
