function InterfaceKnearestPatients
%----------------------------
%InterfaceKnearestPatients :
%This interface can be used on a set of data load on a .mat file
%You have to select a specific patient on the list
% Compatible : MATLAB2013b
%----------------------------

% Cr�ation de l'objet Figure
% fig = figure('units', 'pixels', ...
%     'position', [520 380 300 250], ...
%     'Visible', 'On', ...
%     'name', 'Find k-nearest person');
global startingFolder PatientList h storedStructure
storedStructure=struct();
PatientList='List_of_patients';
startingFolder = 'D:\ANNE_D\PROJETS\SINERGIA\Code\knearestPatients\Matlab';
a=dir('D:\ANNE_D\PROJETS\SINERGIA\Code\knearestPatients\Matlab\*.mat');
matFileList={a.name};
h.fig = figure('Position',[360 50 950 685]);

%%
%%%DATA SELECTION PANEL 
h.DataSelectp = uipanel('Title','Data Selection','FontSize',12,...
    'BackgroundColor','white',...
    'Position',[.05 .85 .90 .15]);
%Panel for the List of enable .mat files
h.Choose_Database_ListSubpanel = uipanel('Parent',h.DataSelectp,'Tag','matFileList','Units','normalized','Position',[.01 .07 .7 0.8]);  
%List of enable .mat files
h.uiplistDB=uicontrol('Parent',h.Choose_Database_ListSubpanel,'Style','popupmenu','String',matFileList,'BackgroundColor','white',...
    'Units','normalized','Position',[.01 .07 .8 .8],'Callback',@ValidateDataBase);
%Button to search other .mat file
h.uiSearchDBbut = uicontrol('Parent',h.DataSelectp,'String','Search other Database',...
              'Units','normalized','Position',[0.75 0.15 0.2 0.5],'callback', @ChangeDataBase);
%%
%%KNP SELECTION PANEL 
h.KNPSelectp = uipanel('Title','Select k-nearest patients parameters','FontSize',12,...
    'BackgroundColor','white',...
    'Position',[.05 .15 .90 .65]);
%%
%Panel for the List of patient
h.Choose_Patient_ListSubpanel = uipanel('Parent',h.KNPSelectp,'Tag','PatientList','Units','normalized','Position',[.01 .85 .35 0.15]);  
%List of patient in the selected .mat file
h.uitextPatlist=uicontrol('Parent',h.Choose_Patient_ListSubpanel,'Style','text','String','Select patient',...
    'Units','normalized','Position',[.01 .07 .3 .7]);
%Adapter le callback, et rendre Enable seulement une fois la base de donnee
%selectionnee
h.uiPatlist=uicontrol('Parent',h.Choose_Patient_ListSubpanel,'Style','popupmenu','String',PatientList,'BackgroundColor','white',...
    'Units','normalized','Position',[.35 .07 .6 .7]);

%Panel for k value
h.Choose_k_ValueSubpanel = uipanel('Parent',h.KNPSelectp,'Tag','PatientList','Units','normalized','Position',[.38 .85 .3 0.15]);  
%List of patient in the selected .mat file
h.uitextk_Value1=uicontrol('Parent',h.Choose_k_ValueSubpanel,'Style','text','String','Select k - number of patients',...
    'Units','normalized','Position',[.01 .07 .6 .7]);
%
% h.uik_Value=uicontrol('Parent',h.Choose_k_ValueSubpanel,'Style','popupmenu','String',{8,9,10},'BackgroundColor','white',...
%     'Units','normalized','Position',[.7 .07 .15 .7]);
h.uik_Value=uicontrol('Parent',h.Choose_k_ValueSubpanel,'Style','slider','Min',1,'Max',20,'Value',10,'BackgroundColor','white',...
    'Units','normalized','Position',[.7 .07 .1 .85],'Callback',@UpdateSliderK);
h.uitextk_Value=uicontrol('Parent',h.Choose_k_ValueSubpanel,'Style','text','String',get(h.uik_Value, 'Value'),...
    'Units','normalized','Position',[.8 .4 .1 .2]);

%Panel for lambda value
h.Choose_lbd_ValueSubpanel = uipanel('Parent',h.KNPSelectp,'Tag','PatientList','Units','normalized','Position',[.69 .85 .3 0.15]);  
%List of patient in the selected .mat file
h.uitextlbd_Value1=uicontrol('Parent',h.Choose_lbd_ValueSubpanel,'Style','text','String','Select lambda - Weight of the derivate/Shape of the curve',...
    'Units','normalized','Position',[.01 .07 .6 .7]);
%h.uilbd_Value=uicontrol('Parent',h.Choose_lbd_ValueSubpanel,'Style','popupmenu','String',{8,9,10},'BackgroundColor','white',...
%    'Units','normalized','Position',[.7 .07 .15 .7]);
h.uilbd_Value=uicontrol('Parent',h.Choose_lbd_ValueSubpanel,'Style','slider','Min',1,'Max',1000,'Value',100,'BackgroundColor','white',...
    'Units','normalized','Position',[.7 .07 .1 .85],'Callback',@UpdateSliderLBD);
h.uitextlbd_Value=uicontrol('Parent',h.Choose_lbd_ValueSubpanel,'Style','text','String',get(h.uilbd_Value, 'Value'),...
    'Units','normalized','Position',[.8 .4 .1 .2]);

%%
%Panel for variable selection
h.Choose_var_SelectSubpanel = uipanel('Parent',h.KNPSelectp,'Title','Select variables of interest','FontSize',8,'Tag','VariableSelection','Units','normalized','Position',[.01 .06 .98 0.78]);  

%Subpanel for the side selection
h.Choose_var_SideSubpanel = uipanel('Parent',h.Choose_var_SelectSubpanel,'Tag','PatientList','Units','normalized','Position',[.01 .9 .5 0.1]); 
h.uitextvar_Side=uicontrol('Parent',h.Choose_var_SideSubpanel,'Style','text','String','Side',...
    'Units','normalized','Position',[.0 .09 .1 .7]);
%
h.uivar_SideL=uicontrol('Parent',h.Choose_var_SideSubpanel,'Style','checkbox','String','Left',...
    'Units','normalized','Position',[.12 .07 .45 .7],'Value',1);
h.uivar_SideR=uicontrol('Parent',h.Choose_var_SideSubpanel,'Style','checkbox','String','Right',...
    'Units','normalized','Position',[.35 .27 .45 .7],'Value',1);


%Sagittal plane selection
h.Choose_var_SagSubpanel = uipanel('Parent',h.Choose_var_SelectSubpanel,'Title','Sagittal plane','Tag','PatientList','Units','normalized','Position',[.01 .05 .2 0.8]); 
%
h.uivar_Thorax1=uicontrol('Parent',h.Choose_var_SagSubpanel,'Style','checkbox','String','Thorax Tilt',...
    'Units','normalized','Position',[.01 .87 .9 .15]);
h.uivar_Pelvic1=uicontrol('Parent',h.Choose_var_SagSubpanel,'Style','checkbox','String','Pelvic Tilt',...
    'Units','normalized','Position',[.01 .67 .9 .15],'Value',1);
h.uivar_Hip1=uicontrol('Parent',h.Choose_var_SagSubpanel,'Style','checkbox','String','Hip Flexion/Extension',...
    'Units','normalized','Position',[.01 .47 .9 .15],'Value',1);
h.uivar_Knee1=uicontrol('Parent',h.Choose_var_SagSubpanel,'Style','checkbox','String','Knee Flexion/Extension',...
    'Units','normalized','Position',[.01 .27 .9 .15],'Value',1);
h.uivar_Ankle1=uicontrol('Parent',h.Choose_var_SagSubpanel,'Style','checkbox','String','Ankle Dors/Plantarflexion',...
    'Units','normalized','Position',[.01 .07 .9 .15],'Value',1);


%Frontal plane selection
h.Choose_var_FrontSubpanel = uipanel('Parent',h.Choose_var_SelectSubpanel,'Title','Frontal plane','Tag','PatientList','Units','normalized','Position',[.23 .05 .2 0.8]); 
%Adapter le callback 
h.uivar_Thorax2=uicontrol('Parent',h.Choose_var_FrontSubpanel,'Style','checkbox','String','Thorax Obliquity',...
    'Units','normalized','Position',[.01 .87 .9 .15]);
h.uivar_Pelvic2=uicontrol('Parent',h.Choose_var_FrontSubpanel,'Style','checkbox','String','Pelvic Obliquity',...
    'Units','normalized','Position',[.01 .67 .9 .15],'Value',1);
h.uivar_Hip2=uicontrol('Parent',h.Choose_var_FrontSubpanel,'Style','checkbox','String','Hip Abb/Abduction',...
    'Units','normalized','Position',[.01 .47 .9 .15],'Value',1);
h.uivar_Knee2=uicontrol('Parent',h.Choose_var_FrontSubpanel,'Style','checkbox','String','Knee Varus/Valgus',...
    'Units','normalized','Position',[.01 .27 .9 .15]);


%Transverse plane selection
h.Choose_var_TransSubpanel = uipanel('Parent',h.Choose_var_SelectSubpanel,'Title','Frontal plane','Tag','PatientList','Units','normalized','Position',[.45 .05 .2 0.8]); 
%Adapter le callback 
h.uivar_Thorax3=uicontrol('Parent',h.Choose_var_TransSubpanel,'Style','checkbox','String','Thorax Rotation',...
    'Units','normalized','Position',[.01 .87 .9 .15]);
h.uivar_Pelvic3=uicontrol('Parent',h.Choose_var_TransSubpanel,'Style','checkbox','String','Pelvic Rotation',...
    'Units','normalized','Position',[.01 .67 .9 .15],'Value',1);
h.uivar_Hip3=uicontrol('Parent',h.Choose_var_TransSubpanel,'Style','checkbox','String','Hip Rotation',...
    'Units','normalized','Position',[.01 .47 .9 .15],'Value',1);
h.uivar_Ankle3=uicontrol('Parent',h.Choose_var_TransSubpanel,'Style','checkbox','String','Foot Progression',...
    'Units','normalized','Position',[.01 .07 .9 .15],'Value',1);

%%
%BUTTON TO COMPUTE THE K-NEAREST PATIENTS ALGO
h.KNP_BUTTON = uicontrol('Parent',h.KNPSelectp,'String',{'Find k-nearest patients'},'FontSize',14,...
    'Units','normalized','Position',[.67 .12 .3 .2],'callback', @FIND_KNP);

set(findall(h.KNPSelectp, '-property', 'enable'), 'enable', 'off');
% R�cup�ration des identifiants utiles
%handles = guidata(h.fig);


%%%%%CALLBACK UPDATE SLIDER LAMBDA
function UpdateSliderLBD (hObject, eventdata, handles)
global h
set(h.uitextlbd_Value,'String',num2str(round(get(h.uilbd_Value, 'Value'),0)));

%%%%%CALLBACK UPDATE SLIDER k
function UpdateSliderK (hObject, eventdata, handles)
global h
set(h.uitextk_Value,'String',num2str(round(get(h.uik_Value, 'Value'),0)));


%%
function ChangeDataBase(hObject, eventdata, handles)
    %%%%%%%%%%LOAD DATA PART%%%%%%%%%%%%%%%%%
% Have user browse for a file, from a specified "starting folder."
% For convenience in browsing, set a starting folder from which to browse.
   global startingFolder PatientList h storedStructure;
if ~exist(startingFolder, 'dir')
  % If that folder doesn't exist, just start in the current folder.
  startingFolder = pwd;
end
% Get the name of the mat file that the user wants to use.
defaultFileName = fullfile(startingFolder, '*.mat');
[baseFileName, folder] = uigetfile(defaultFileName, 'Select a mat file');
if baseFileName == 0
  % User clicked the Cancel button.
  return;
end
fullFileName = fullfile(folder, baseFileName);
storedStructure = load(fullFileName);

set(h.uivar_Thorax1,'enable', 'off');
set(h.uivar_Thorax2,'enable', 'off');
set(h.uivar_Thorax3,'enable', 'off');
set(h.uivar_Knee2,'enable', 'off');
set(findall(h.KNPSelectp, '-property', 'enable'), 'enable', 'on');


if strcmp('MEAN_PAT_',baseFileName(1:9))
    PatientList={storedStructure.Patient};
    set(h.uiPatlist,'String',(cell2mat(PatientList)));

else
     PatientList={storedStructure.(baseFileName(1:end-4)).Patient};
%%%Calcule les valeurs moyennes par patients/visite de la base de donnees
dataB=storedStructure.(baseFileName(1:end-4));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nbcycle=0;
NEWDB=struct();
patlistunique=unique(cell2mat(PatientList));
nbPat=1;
currentpat=patlistunique(nbPat);
FF=fieldnames(dataB.X);
for ii=1:length(dataB.Patient)
    id_pat=getfield(dataB,'Patient',{ii});
    if ~isempty(intersect(currentpat,id_pat))
        nbcycle=nbcycle+1;  
    else
        currentpat=id_pat;
        nbcycle=1;
    end
    for v=1:length(FF)
        NEWDB.(['P' num2str(id_pat)]).(FF{v}).(['Cycle_' num2str(nbcycle)])=dataB.X.(FF{v})(ii,:);
    end
end

NEWDB;
ReshapeStructure=struct();
P=fieldnames(NEWDB);
ppp=cell(length(P),1);
for p=1:length(P)
    pp=char(P{p});
    ppp{p}=pp(2:end);
end
for i=1:length(P)
    ReshapeStructure.Patient=char(ppp);  
    for v=1:length(FF)
        NBC=fieldnames(NEWDB.(char(P(i))).(FF{v}));
        SUM=NEWDB.(char(P(i))).(FF{v}).(char(NBC{1}));
        for j=2:length(NBC)
            SUM=SUM+NEWDB.(char(P(i))).(FF{v}).(char(NBC{j}));
        end
        SUM=SUM/length(NBC);
        ReshapeStructure.X.(FF{v})(i,:)=SUM;
    end
end
save([startingFolder '\MEAN_PAT_' baseFileName], '-struct', 'ReshapeStructure');
storedStructure=ReshapeStructure;
set(h.uiPatlist,'String',unique(cell2mat(PatientList)));

end


%%
function ValidateDataBase(hObject, eventdata, handles)
    %%%%%%%%%%LOAD DATA PART%%%%%%%%%%%%%%%%%
% Have user browse for a file, from a specified "starting folder."
% For convenience in browsing, set a starting folder from which to browse.

  %str = get(handle.change,'String');
  %%%A adapter, prend le premier de la liste pour le moment
  items = get(hObject,'String');
  index_selected = get(hObject,'Value');
 selectedItem = items{index_selected};
    global startingFolder PatientList h storedStructure;
fullFileName = fullfile(startingFolder, selectedItem);
storedStructure = load(fullFileName);

set(findall(h.KNPSelectp, '-property', 'enable'), 'enable', 'on');
set(h.uivar_Thorax1,'enable', 'off');
set(h.uivar_Thorax2,'enable', 'off');
set(h.uivar_Thorax3,'enable', 'off');
set(h.uivar_Knee2,'enable', 'off');


if strcmp('MEAN_PAT_',selectedItem(1:9))
    PatientList={storedStructure.Patient};
    set(h.uiPatlist,'String',(cell2mat(PatientList)));

else
     PatientList={storedStructure.(selectedItem(1:end-4)).Patient};
%%%Calcule les valeurs moyennes par patients/visite de la base de donnees
dataB=storedStructure.(selectedItem(1:end-4));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nbcycle=0;
NEWDB=struct();
patlistunique=unique(cell2mat(PatientList));
nbPat=1;
currentpat=patlistunique(nbPat);
FF=fieldnames(dataB.X);
for ii=1:length(dataB.Patient)
    id_pat=getfield(dataB,'Patient',{ii});
    if ~isempty(intersect(currentpat,id_pat))
        nbcycle=nbcycle+1;  
    else
        currentpat=id_pat;
        nbcycle=1;
    end
    for v=1:length(FF)
        NEWDB.(['P' num2str(id_pat)]).(FF{v}).(['Cycle_' num2str(nbcycle)])=dataB.X.(FF{v})(ii,:);
    end
end

NEWDB;
ReshapeStructure=struct();
P=fieldnames(NEWDB);
ppp=cell(length(P),1);
for p=1:length(P)
    pp=char(P{p});
    ppp{p}=pp(2:end);
end
for i=1:length(P)
    ReshapeStructure.Patient=char(ppp); 
    for v=1:length(FF)
        NBC=fieldnames(NEWDB.(char(P(i))).(FF{v}));
        SUM=NEWDB.(char(P(i))).(FF{v}).(char(NBC{1}));
        for j=2:length(NBC)
            SUM=SUM+NEWDB.(char(P(i))).(FF{v}).(char(NBC{j}));
        end
        SUM=SUM/length(NBC);
        ReshapeStructure.X.(FF{v})(i,:)=SUM;
    end
end
save([startingFolder '\MEAN_PAT_' selectedItem], '-struct', 'ReshapeStructure');
storedStructure=ReshapeStructure;
set(h.uiPatlist,'String',unique(cell2mat(PatientList)));

end


%%
function FIND_KNP(hObject, eventdata, handles)
 %%%%%%%%%%FIND K-NEAREST PATIENTS%%%%%%%%%%%%%%%%%
 global h storedStructure;
 model=storedStructure;
field=fieldnames(model);
%model=model.(field{1});

%%%FIND SELECTED VARIABLES
VariableList={'RPelvisAngles_1';'LPelvisAngles_1';
    'RHipAngles_1';'LHipAngles_1';
    'RKneeAngles_1';'LKneeAngles_1';
    'RAnkleAngles_1';'LAnkleAngles_1';
    'RPelvisAngles_2';'LPelvisAngles_2';
    'RHipAngles_2';'LHipAngles_2';
    %'RKneeAngles_2';'LKneeAngles_2';
    'RPelvisAngles_3';'LPelvisAngles_3';
    'RHipAngles_3';'LHipAngles_3';
    'RFootProgressAngles_3';'LFootProgressAngles_3';
    };

VariableBoolean={get(h.uivar_Pelvic1,'Value') && get(h.uivar_SideR,'Value');get(h.uivar_Pelvic1,'Value') && get(h.uivar_SideL,'Value');
   get(h.uivar_Hip1,'Value') && get(h.uivar_SideR,'Value');get(h.uivar_Hip1,'Value') && get(h.uivar_SideL,'Value');
    get(h.uivar_Knee1,'Value') && get(h.uivar_SideR,'Value');get(h.uivar_Knee1,'Value') && get(h.uivar_SideL,'Value');
    get(h.uivar_Ankle1,'Value') && get(h.uivar_SideR,'Value');get(h.uivar_Ankle1,'Value') && get(h.uivar_SideL,'Value');
    get(h.uivar_Pelvic2,'Value') && get(h.uivar_SideR,'Value');get(h.uivar_Pelvic2,'Value') && get(h.uivar_SideL,'Value');
   get(h.uivar_Hip2,'Value') && get(h.uivar_SideR,'Value');get(h.uivar_Hip2,'Value') && get(h.uivar_SideL,'Value');
   % get(h.uivar_Knee2,'Value') && get(h.uivar_SideR,'Value');get(h.uivar_Knee2,'Value') && get(h.uivar_SideL,'Value');
     get(h.uivar_Pelvic3,'Value') && get(h.uivar_SideR,'Value');get(h.uivar_Pelvic3,'Value') && get(h.uivar_SideL,'Value');
   get(h.uivar_Hip3,'Value') && get(h.uivar_SideR,'Value');get(h.uivar_Hip3,'Value') && get(h.uivar_SideL,'Value');
 get(h.uivar_Ankle3,'Value') && get(h.uivar_SideR,'Value');get(h.uivar_Ankle3,'Value') && get(h.uivar_SideL,'Value');
    };
VariableBoolean=cell2mat(VariableBoolean);
%Compare the lists to select the variables of interest

if sum(VariableBoolean)>0
    B=table(VariableList,logical(zeros(length(VariableList),1)));
    B.Properties.VariableNames = {'VariableList' 'VariableBoolean'};
    A=table(VariableList,VariableBoolean);
    VarSelected=setdiff(A,B);
else
    disp('Please tick at least one variable');
end
%%%FIND SELECTED PATIENT
itemsPatients = cellstr(get(h.uiPatlist,'String'));
  index_PatSelected = get(h.uiPatlist,'Value');
 PatSelectedID = itemsPatients{(index_PatSelected)};
 
 %%%FIND SELECTED k value
% itemskValues = cellstr(get(h.uik_Value,'String'));
%   index_kSelected = get(h.uik_Value,'Value');
%  kSelectedID = itemskValues{(index_kSelected)};
  kSelectedID = num2str(round(get(h.uik_Value, 'Value'),0));
 
 %%%FIND SELECTED lambda value
% itemslbdValues = cellstr(get(h.uilbd_Value,'String'));
%   index_lbdSelected = get(h.uilbd_Value,'Value');
% lbdSelectedID = itemskValues{(index_lbdSelected)};
 lbdSelectedID = num2str(round(get(h.uilbd_Value, 'Value'),0));
 
Y=model.X;
% F=fieldnames(Y);
% F={'RKneeAngles_1','RAnkleAngles_1','RHipAngles_1'};
F=table2array(VarSelected(:,'VariableList'));
Y2=[];
for i=1:length(F)
   % Y2.(F{i})=Y.(F{i})(1,:);
   Y2.(F{i})=Y.(F{i})(index_PatSelected,:);
end
%Yid=model.Y;
%[Id,dist]=FindKNearestPerson(model,Y2,10,10);
[Id,dist]=FindKNearestPerson(model,Y2,str2num(kSelectedID),str2num(lbdSelectedID));
Id{1} = Id{1}(Id{1}~=index_PatSelected);


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 global f1 f2
 close(f1);close(f2);
f1=figure('Position',[60 50 1350 685]);
t=[0:100];
suptitle({['ID patient= ' num2str(PatSelectedID) ' -:- ' kSelectedID '-nearest patients -:- lambda=' lbdSelectedID] ;''});
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Subplot of Pelvis angle- sagital plane
subplot(4,6,1)
ylimit=[-30;30];
if sum(strcmp('LPelvisAngles_1',F))>0
    plot(t,Y.LPelvisAngles_1(Id{1},:)',t,Y2.LPelvisAngles_1','g-.','LineWidth',4)
    ylim(ylimit)
    title('Pelvis Tilt')
    xlabel('Left ')
else
    plot(t,model.X.LPelvisAngles_1(Id{1},:)',t,model.X.LPelvisAngles_1(index_PatSelected,:)','g-.','LineWidth',1)
     ylim(ylimit)
    title('Pelvis Tilt ')
    xlabel('Left ')
end
subplot(4,6,4)
if sum(strcmp('RPelvisAngles_1',F))>0
plot(t,Y.RPelvisAngles_1(Id{1},:)',t,Y2.RPelvisAngles_1','g-.','LineWidth',4)
 ylim(ylimit)
title('Pelvis Tilt ')
xlabel('Right ')
else
     plot(t,model.X.RPelvisAngles_1(Id{1},:)',t,model.X.RPelvisAngles_1(index_PatSelected,:)','g-.','LineWidth',1)
      ylim(ylimit)
     title('Pelvis Tilt ')
xlabel('Right ')
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Subplot of Pelvis angle- frontal plane
subplot(4,6,2)
ylimit=[-30;30];
if sum(strcmp('LPelvisAngles_2',F))>0
    plot(t,Y.LPelvisAngles_2(Id{1},:)',t,Y2.LPelvisAngles_2','g-.','LineWidth',4)
     ylim(ylimit)
    title('Pelvis Obliquity')
    xlabel('Left ')
else
    plot(t,model.X.LPelvisAngles_2(Id{1},:)',t,model.X.LPelvisAngles_2(index_PatSelected,:)','g-.','LineWidth',1)
     ylim(ylimit)
    title('Pelvis Obliquity')
    xlabel('Left ')
end
subplot(4,6,5)
if sum(strcmp('RPelvisAngles_2',F))>0
plot(t,Y.RPelvisAngles_2(Id{1},:)',t,Y2.RPelvisAngles_2','g-.','LineWidth',4)
 ylim(ylimit)
 title('Pelvis Obliquity')
xlabel('Right ')
else
     plot(t,model.X.RPelvisAngles_2(Id{1},:)',t,model.X.RPelvisAngles_2(index_PatSelected,:)','g-.','LineWidth',1)
      ylim(ylimit)
      title('Pelvis Obliquity')
xlabel('Right ')
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Subplot of Pelvis angle- transverse plane
subplot(4,6,3)
ylimit=[-30;30];
if sum(strcmp('LPelvisAngles_3',F))>0
    plot(t,Y.LPelvisAngles_3(Id{1},:)',t,Y2.LPelvisAngles_3','g-.','LineWidth',4)
     ylim(ylimit)
    title('Pelvis Rotation')
    xlabel('Left ')
else
    plot(t,model.X.LPelvisAngles_3(Id{1},:)',t,model.X.LPelvisAngles_3(index_PatSelected,:)','g-.','LineWidth',1)
     ylim(ylimit)
    title('Pelvis Rotation')
    xlabel('Left ')
end
subplot(4,6,6)
if sum(strcmp('RPelvisAngles_3',F))>0
plot(t,Y.RPelvisAngles_3(Id{1},:)',t,Y2.RPelvisAngles_3','g-.','LineWidth',4)
 ylim(ylimit)
title('Pelvis Rotation')
xlabel('Right ')
else
     plot(t,model.X.RPelvisAngles_3(Id{1},:)',t,model.X.RPelvisAngles_3(index_PatSelected,:)','g-.','LineWidth',1)
      ylim(ylimit)
     title('Pelvis Rotation')
xlabel('Right ')
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Subplot of Hip angle- Sagital plane
subplot(4,6,7)
ylimit=[-30;60];
if sum(strcmp('LHipAngles_1',F))>0
    plot(t,Y.LHipAngles_1(Id{1},:)',t,Y2.LHipAngles_1','g-.','LineWidth',4)
     ylim(ylimit)
    title('Hip Flexion/Extension')
    xlabel('Left ')
else
    plot(t,model.X.LHipAngles_1(Id{1},:)',t,model.X.LHipAngles_1(index_PatSelected,:)','g-.','LineWidth',1)
     ylim(ylimit)
    title('Hip Flexion/Extension')
    xlabel('Left ')
end
subplot(4,6,10)
if sum(strcmp('RHipAngles_1',F))>0
plot(t,Y.RHipAngles_1(Id{1},:)',t,Y2.RHipAngles_1','g-.','LineWidth',4)
 ylim(ylimit)
 title('Hip Flexion/Extension')
xlabel('Right ')
else
     plot(t,model.X.RHipAngles_1(Id{1},:)',t,model.X.RHipAngles_1(index_PatSelected,:)','g-.','LineWidth',1)
      ylim(ylimit)
      title('Hip Flexion/Extension')
xlabel('Right ')
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Subplot of Hip angle- frontal plane
subplot(4,6,8)
ylimit=[-30;30];
if sum(strcmp('LHipAngles_2',F))>0
    plot(t,Y.LHipAngles_2(Id{1},:)',t,Y2.LHipAngles_2','g-.','LineWidth',4)
     ylim(ylimit)
    title('Hip Obliquity')
    xlabel('Left ')
else
    plot(t,model.X.LHipAngles_2(Id{1},:)',t,model.X.LHipAngles_2(index_PatSelected,:)','g-.','LineWidth',1)
     ylim(ylimit)
    title('Hip Obliquity')
    xlabel('Left ')
end
subplot(4,6,11)
if sum(strcmp('RHipAngles_2',F))>0
plot(t,Y.RHipAngles_2(Id{1},:)',t,Y2.RHipAngles_2','g-.','LineWidth',4)
 ylim(ylimit)
title('Hip Obliquity')
xlabel('Right ')
else
     plot(t,model.X.RHipAngles_2(Id{1},:)',t,model.X.RHipAngles_2(index_PatSelected,:)','g-.','LineWidth',1)
      ylim(ylimit)
     title('Hip Obliquity')
xlabel('Right ')
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Subplot of Hip angle- transverse plane
subplot(4,6,9)
ylimit=[-40;40];
if sum(strcmp('LHipAngles_3',F))>0
    plot(t,Y.LHipAngles_3(Id{1},:)',t,Y2.LHipAngles_3','g-.','LineWidth',4)
     ylim(ylimit)
    title('Hip Rotation')
    xlabel('Left ')
else
    plot(t,model.X.LHipAngles_3(Id{1},:)',t,model.X.LHipAngles_3(index_PatSelected,:)','g-.','LineWidth',1)
     ylim(ylimit)
    title('Hip Rotation')
    xlabel('Left ')
end
subplot(4,6,12)
if sum(strcmp('RHipAngles_3',F))>0
plot(t,Y.RHipAngles_3(Id{1},:)',t,Y2.RHipAngles_3','g-.','LineWidth',4)
 ylim(ylimit)
title('Hip Rotation')
xlabel('Right ')
else
     plot(t,model.X.RHipAngles_3(Id{1},:)',t,model.X.RHipAngles_3(index_PatSelected,:)','g-.','LineWidth',1)
      ylim(ylimit)
     title('Hip Rotation')
xlabel('Right ')
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Subplot of Knee angle- Sagital plane
subplot(4,6,13)
ylimit=[-20;80];
if sum(strcmp('LKneeAngles_1',F))>0
    plot(t,Y.LKneeAngles_1(Id{1},:)',t,Y2.LKneeAngles_1','g-.','LineWidth',4)
     ylim(ylimit)
    title('Knee Flexion/Extension')
    xlabel('Left ')
else
    plot(t,model.X.LKneeAngles_1(Id{1},:)',t,model.X.LKneeAngles_1(index_PatSelected,:)','g-.','LineWidth',1)
     ylim(ylimit)
    title('Knee Flexion/Extension')
    xlabel('Left ')
end
subplot(4,6,16)
if sum(strcmp('RKneeAngles_1',F))>0
plot(t,Y.RKneeAngles_1(Id{1},:)',t,Y2.RKneeAngles_1','g-.','LineWidth',4)
 ylim(ylimit)
 title('Knee Flexion/Extension')
xlabel('Right ')
else
     plot(t,model.X.RKneeAngles_1(Id{1},:)',t,model.X.RKneeAngles_1(index_PatSelected,:)','g-.','LineWidth',1)
      ylim(ylimit)
      title('Knee Flexion/Extension')
xlabel('Right ')
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %Subplot of Knee angle- Frontal plane
% subplot(4,6,14)
%ylimit=[-30;30];
% if sum(strcmp('LKneeAngles_2',F))>0
%     plot(t,Y.LKneeAngles_2(Id{1},:)',t,Y2.LKneeAngles_2','g-.','LineWidth',4)
% ylim(ylimit)
%     title('Knee Valgus/Varus')
%     xlabel('Left ')
% else
%     plot(t,model.X.LKneeAngles_2(Id{1},:)',t,model.X.LKneeAngles_2(index_PatSelected,:)','g-.','LineWidth',1)
% ylim(ylimit)
%     title('Knee Valgus/Varus')
%     xlabel('Left ')
% end
% subplot(4,6,17)
% if sum(strcmp('RKneeAngles_2',F))>1
% plot(t,Y.RKneeAngles_2(Id{1},:)',t,Y2.RKneeAngles_2','g-.','LineWidth',4)
% ylim(ylimit)
%  title('Knee Valgus/Varus')
% xlabel('Right ')
% else
%      plot(t,model.X.RKneeAngles_2(Id{1},:)',t,model.X.RKneeAngles_2(index_PatSelected,:)','g-.','LineWidth',1)
% ylim(ylimit)
%       title('Knee Valgus/Varus')
% xlabel('Right ')
% end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Subplot of Ankle angle- sagital plane
subplot(4,6,19)
ylimit=[-30;20];
if sum(strcmp('LAnkleAngles_1',F))>0
    plot(t,Y.LAnkleAngles_1(Id{1},:)',t,Y2.LAnkleAngles_1','g-.','LineWidth',4)
     ylim(ylimit)
    title('Ankle Dorsi/Plantar-flexion')
    xlabel('Left ')
else
    plot(t,model.X.LAnkleAngles_1(Id{1},:)',t,model.X.LAnkleAngles_1(index_PatSelected,:)','g-.','LineWidth',1)
     ylim(ylimit)
    title('Ankle Dorsi/Plantar-flexion')
    xlabel('Left ')
end
subplot(4,6,22)
if sum(strcmp('RAnkleAngles_1',F))>0
plot(t,Y.RAnkleAngles_1(Id{1},:)',t,Y2.RAnkleAngles_1','g-.','LineWidth',4)
 ylim(ylimit)
title('Ankle Dorsi/Plantar-flexion')
xlabel('Right ')
else
     plot(t,model.X.RAnkleAngles_1(Id{1},:)',t,model.X.RAnkleAngles_1(index_PatSelected,:)','g-.','LineWidth',1)
      ylim(ylimit)
     title('Ankle Dorsi/Plantar-flexion')
xlabel('Right ')
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Subplot of Foot progression
subplot(4,6,21)
ylimit=[-30;30];
if sum(strcmp('LFootProgressAngles_3',F))>0
    plot(t,Y.LFootProgressAngles_3(Id{1},:)',t,Y2.LFootProgressAngles_3','g-.','LineWidth',4)
     ylim(ylimit)
    title('Foot progression')
    xlabel('Left ')
else
    plot(t,model.X.LFootProgressAngles_3(Id{1},:)',t,model.X.LFootProgressAngles_3(index_PatSelected,:)','g-.','LineWidth',1)
     ylim(ylimit)
    title('Foot progression')
    xlabel('Left ')
end
subplot(4,6,24)
if sum(strcmp('RFootProgressAngles_3',F))>0
plot(t,Y.RFootProgressAngles_3(Id{1},:)',t,Y2.RFootProgressAngles_3','g-.','LineWidth',4)
 ylim(ylimit)
title('Foot progression')
xlabel('Right ')
else
     plot(t,model.X.RFootProgressAngles_3(Id{1},:)',t,model.X.RFootProgressAngles_3(index_PatSelected,:)','g-.','LineWidth',1)
      ylim(ylimit)
     title('Foot progression')
xlabel('Right ')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 line([0.5 0.5], get(gca, 'ylim'));
%cr�e une l�gende avec les couleurs associ�es aux k-patients
% Construct a Legend with the data from the sub-plots
 ListPalALLID=[cell2mat(Id),index_PatSelected];
 ListPalALL=str2double({itemsPatients{ListPalALLID}});
hL = legend(sprintfc('%d',ListPalALL));
% Programatically move the Legend
newPosition = [0.01 0.04 0.1 0.3];
newUnits = 'normalized';
set(hL,'Position', newPosition,'Units', newUnits);

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%EXPORT K-PATIENTS INFOS FROM FILEMAKER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %% (2) Find the anonymised parameters (ID_FM,ID_Visite,Date_Visite)
        %%%%Link with Filemaker%%%%%%%
        connection_time = 1;
        % trying to connect to the database
        conn =  Connect2DBWithoutBDA(connection_time);
        
        %ListPalALL=[str2num(PatSelectedID), cell2mat(Id)];
        %%Patient selected INFO        
        PatSelected=struct();
        
        for pp=1:length(ListPalALL)
        PatSelected(pp).ID=num2str(ListPalALL(pp));
       
         %Identification of ID_Visite and DateVisite associated to
        %ID_Patient in filemaker
        PatSelectedvisite_retrieval_query = horzcat('SELECT ID_Patient,ID_Visite,DateVisite,Age FROM Visite WHERE ID_Patient = ',num2str(ListPalALL(pp)));
        visite_data = fetch(conn,PatSelectedvisite_retrieval_query);
        Age_Lastvisit=visite_data{:,4};
        N=findstr('-',Age_Lastvisit);
        PatSelected(pp).AgeLastVisit=num2str(Age_Lastvisit(1:N-1));

        patient_retrieval_query = horzcat('SELECT ID_Patient,Sexe FROM Patient WHERE ID_Patient = ',num2str(ListPalALL(pp)));
        PatSelected_data = fetch(conn,patient_retrieval_query);
        PatSelected(pp).Sexe=num2str(PatSelected_data{2});
        ss=size(visite_data);
        PatSelected(pp).Nbvisite=num2str(ss(1));
        PatSelected(pp).DateVisites=[sprintf('%s; ',visite_data{1:end-1,3}),visite_data{end,3}];
        
        patient_retrieval_query = horzcat('SELECT ID_Patient,Diagnostic FROM Medical WHERE ID_Patient = ',num2str(ListPalALL(pp)));
        PatSelected_data = fetch(conn,patient_retrieval_query);
        PatSelected(pp).Diagnostic=[sprintf('%s; ',PatSelected_data{1:end-1,2}),PatSelected_data{end,2}];
       
        end
        
       
f2=figure('Position',[760 350 750 385]);
% LastName = {'Smith';'Johnson';'Williams';'Jones';'Brown'};
% Age = [38;43;38;40;49];
% Height = [71;69;64;67;64];
% Weight = [176;163;131;133;119];
%T = table(Age,Height,Weight,'RowNames',LastName);
T=struct2table(PatSelected);
T.Properties.RowNames=T.ID;
T.ID=[];
uitable(f2,'Data',T{:,:},'ColumnName',T.Properties.VariableNames,...
    'RowName',T.Properties.RowNames,'Units', 'Normalized', 'Position',[0, 0, 1, 1]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
